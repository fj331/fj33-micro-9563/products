package com.github.fwfurtado.allbuy.products.shared;

import com.github.fwfurtado.allbuy.products.shared.domain.Product;
import com.github.fwfurtado.allbuy.products.listing.ListingRepository;
import com.github.fwfurtado.allbuy.products.register.RegisterRepository;
import org.springframework.data.repository.Repository;

public interface ProductRepository extends Repository<Product, Long>, ListingRepository, RegisterRepository {
}
