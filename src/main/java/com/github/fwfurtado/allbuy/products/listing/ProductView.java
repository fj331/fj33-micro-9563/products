package com.github.fwfurtado.allbuy.products.listing;

import java.math.BigDecimal;
import java.util.List;

public interface ProductView {

    String getName();
    String getSlug();
    String getCover();
    List<String> getPictures();
    Long getInStock();
    BigDecimal getPrice();

}
