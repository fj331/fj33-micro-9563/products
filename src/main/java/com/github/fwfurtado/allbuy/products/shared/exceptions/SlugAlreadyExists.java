package com.github.fwfurtado.allbuy.products.shared.exceptions;

public class SlugAlreadyExists extends IllegalStateException {
    public SlugAlreadyExists(String msg) {
        super(msg);
    }
}
