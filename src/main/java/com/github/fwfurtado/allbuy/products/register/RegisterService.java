package com.github.fwfurtado.allbuy.products.register;

import com.github.fwfurtado.allbuy.products.shared.exceptions.InvalidPictureException;
import com.github.fwfurtado.allbuy.products.shared.exceptions.ProductAlreadyExist;
import com.github.fwfurtado.allbuy.products.shared.exceptions.SlugAlreadyExists;
import org.springframework.stereotype.Service;

@Service
public class RegisterService {


    private final RegisterRepository repository;
    private final RegisteredFormToProduct mapper;

    public RegisterService(RegisterRepository repository, RegisteredFormToProduct mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public String registerNewProductFrom(RegisteredForm form) {
        ensureThatThereIsNoOtherSimilarProduct(form);
        ensureThatTheCoverIsInPictures(form);

        var product = mapper.map(form);

        repository.save(product);

        return product.getSlug();
    }

    private void ensureThatTheCoverIsInPictures(RegisteredForm form) {
        var cover = form.getCover();
        var pictures = form.getPictures();

        pictures.stream().filter(cover::equals).findFirst().orElseThrow(() -> new InvalidPictureException("The cover is not in pictures"));
    }

    private void ensureThatThereIsNoOtherSimilarProduct(RegisteredForm form) {
        repository.findByBarcode(form.getBarcode()).ifPresent((p) -> {throw new ProductAlreadyExist("Already exists a product with same barcode");});
        repository.findBySlug(form.getSlug()).ifPresent((p) -> {throw new SlugAlreadyExists("Already exists a product with same slug");});
    }
}
