package com.github.fwfurtado.allbuy.products.register;

import com.github.fwfurtado.allbuy.products.shared.domain.Product;

import java.util.Optional;

public interface RegisterRepository {
    Optional<Product> findByBarcode(String barcode);
    Optional<Product> findBySlug(String slug);

    void save(Product product);
}
