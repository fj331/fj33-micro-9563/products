package com.github.fwfurtado.allbuy.products.register;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
public class RegisterController {

    private final RegisterService service;

    public RegisterController(RegisterService service) {
        this.service = service;
    }

    @PostMapping("products")
    ResponseEntity<?> register(@RequestBody RegisteredForm form) {
        var slug = service.registerNewProductFrom(form);

        var uri = URI.create("/products/" + slug);
        return ResponseEntity.created(uri).build();
    }

}
