package com.github.fwfurtado.allbuy.products.shared.exceptions;

public class InvalidPictureException extends IllegalArgumentException {
    public InvalidPictureException(String msg) {
        super(msg);
    }
}
