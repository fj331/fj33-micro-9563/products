package com.github.fwfurtado.allbuy.products.listing;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class ListingController {

    private final ListingRepository repository;

    public ListingController(ListingRepository repository) {
        this.repository = repository;
    }

    @GetMapping("products/{slug}")
    Optional<ProductView> show(@PathVariable String slug) {
        return repository.filterProductBySlug(slug);
    }
}
