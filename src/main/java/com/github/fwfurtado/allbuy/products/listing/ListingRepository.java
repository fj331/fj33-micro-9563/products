package com.github.fwfurtado.allbuy.products.listing;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ListingRepository {
    @Query("select p from Product p where p.slug = :slug")
    Optional<ProductView> filterProductBySlug(@Param("slug") String slug);
}
