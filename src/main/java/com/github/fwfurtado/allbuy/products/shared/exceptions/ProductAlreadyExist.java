package com.github.fwfurtado.allbuy.products.shared.exceptions;

public class ProductAlreadyExist extends IllegalStateException{
    public ProductAlreadyExist(String msg) {
        super(msg);
    }
}
