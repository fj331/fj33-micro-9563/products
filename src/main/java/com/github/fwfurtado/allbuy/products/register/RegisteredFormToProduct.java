package com.github.fwfurtado.allbuy.products.register;

import com.github.fwfurtado.allbuy.products.shared.domain.Product;
import org.springframework.stereotype.Component;

@Component
public class RegisteredFormToProduct {
    public Product map(RegisteredForm form) {
        return new Product(form.getName(), form.getBarcode(), form.getSlug(), form.getCover(), form.getPrice(), form.getPictures(), 0L);
    }
}
