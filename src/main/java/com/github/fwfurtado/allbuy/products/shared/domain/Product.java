package com.github.fwfurtado.allbuy.products.shared.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String barcode;
    private String slug;
    private String cover;
    private BigDecimal price;
    @ElementCollection
    @CollectionTable(name = "product_pictures")
    private List<String> pictures = new ArrayList<>();

    private Long inStock;

    Product() {}

    public Product(String name, String barcode, String slug, String cover, BigDecimal price, List<String> pictures, Long inStock) {
        this.name = name;
        this.barcode = barcode;
        this.slug = slug;
        this.cover = cover;
        this.price = price;
        this.pictures = pictures;
        this.inStock = inStock;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getSlug() {
        return slug;
    }

    public String getCover() {
        return cover;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public Long getInStock() {
        return inStock;
    }
}
