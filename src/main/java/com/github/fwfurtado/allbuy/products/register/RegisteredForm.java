package com.github.fwfurtado.allbuy.products.register;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RegisteredForm {
    private String name;
    private String barcode;
    private String slug;
    private String cover;
    private List<String> pictures = new ArrayList<>();
    private BigDecimal price;

    public BigDecimal getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public String getCover() {
        return cover;
    }

    public List<String> getPictures() {
        return pictures;
    }

    public String getBarcode() {
        return barcode;
    }
}
